VERSION = 1.0

.PHONY: all clean

all: CONFIG
	rm -rf include lib obj
	mkdir -p include lib
	(cd Tawa; make -f Makefile install INCLUDES=${})
	(cd apps; make -f Makefile)

clean:
	(cd Tawa; make -f Makefile clean)
	(cd apps; make -f Makefile clean)
	(rm -rf include lib obj)
	(find . -name "core" -exec rm {} \;)
	(find . -name "*~" -exec rm {} \;)

CONFIG:
	echo "HOME = ${PWD}" > $@
	cat DEFAULT_CONFIG >> $@

